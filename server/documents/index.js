module.exports = ({ name, price1, price2, receiptId }) => {
    const today = new Date();
return `
    <!doctype html>
    <html>
       <head>
          <meta charset="utf-8">
          <title>prescription</title>
          <style>
             .prescription {
             max-width: 900px;
             margin: auto;
             padding: 30px;
             border: 1px solid #eee;
             box-shadow: 0 0 10px rgba(0, 0, 0, .15);
             font-size: 16px;
             line-height: 24px;
             font-family: 'Helvetica Neue', 'Helvetica',
             color: #555;
             }
             .margin-top {
             margin-top: 50px;
             }
             .justify-center {
             text-align: center;
             }
             .prescription table {
             width: 100%;
             line-height: inherit;
             text-align: left;
             }
             .prescription table td {
             padding: 5px;
             vertical-align: top;
             }
             .prescription table tr td:nth-child(2) {
             text-align: right;
             }
             .prescription table tr.top table td {
             padding-bottom: 5px;
             }
             .prescription table tr.top table td.title p {
               font-size: 16px;
               margin-bottom:0;
               color: #333;
             }
             .prescription table tr.information table td {
             padding-bottom: 40px;
             }
             .prescription table tr.heading td {
             background: #eee;
             border-bottom: 1px solid #ddd;
             font-weight: bold;
             }
             .prescription table tr.details td {
             padding-bottom: 20px;
             }
             .prescription table tr.item td {
             border-bottom: 1px solid #eee;
             }
             .prescription table tr.item.last td {
             border-bottom: none;
             }
             .prescription table tr.total td:nth-child(2) {
             border-top: 2px solid #eee;
             font-weight: bold;
             }
             @media only screen and (max-width: 600px) {
             .prescription table tr.top table td {
             width: 100%;
             display: block;
             text-align: center;
             }
             .prescription table tr.information table td {
             width: 100%;
             display: block;
             text-align: center;
             }
             }
          </style>
       </head>
       <body>
          <div class="prescription">
             <table cellpadding="0" cellspacing="0">
                <tr class="top">
                   <td colspan="2">
                      <table>
                         <tr>
                            <td class="title">
                              <span>Dr. Jubaidul Islam</span> <br/>
                              <span>Dr. Jubaidul Islam</span> <br/>
                              <span>Dr. Jubaidul Islam</span> <br/>
                              <span>Dr. Jubaidul Islam</span><br/>
                            </td>
                            <td>
                               logo
                            </td>
                            <td>
                              <span>Dr. Jubaidul Islam</span><br/>
                              <span>Dr. Jubaidul Islam</span><br/>
                              <span>Dr. Jubaidul Islam</span><br/>
                              <span>Dr. Jubaidul Islam</span><br/>
                            </td>
                         </tr>
                      </table>
                   </td>
                </tr>
                <tr class="information">
                   <td>
                      <table>
                         <tr>
                            <td>
                              Name: ${name}
                            </td>
                            <td>
                              Year: ${receiptId}
                            </td>
                            <td>
                              Date: ${`${today.getDate()}/ ${today.getMonth() + 1}/ ${today.getFullYear()}`}
                            </td>
                         </tr>
                      </table>
                   </td>
                </tr>
                <tr class="heading">
                   <td>Bought items:</td>
                   <td>Price</td>
                </tr>
                <tr class="item">
                   <td> First item: </td>
                   <td>${price1}$</td>
                </tr>
                <tr class="item">
                   <td>Second item:</td>
                   <td>${price2}$</td>
                </tr>
             </table>
             <br />
             <h1 class="justify-center">Total price: ${parseInt(price1) + parseInt(price2)}$</h1>
          </div>
       </body>
    </html>
    `;
};